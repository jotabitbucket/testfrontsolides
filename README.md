**Sitesma de cadastro utilizando node.js e react.js**

Para o desenvolviemento deste sistema, criou-se uma api com node.js para serem consumidas no front. 
Utilizou-se o MYSQL como fonte de persistência de dados, com duas tabelas, tabela de usuários e tabela de registro, fazendo realções entre elas
utlizando o ID de usuário como foreign key para relacionar-se com a tabela de registro. A conxexão local com o banco de dados está configurada
no arquivo db.js

---

## Divisão do projeto.
Dividiu-se o projeto em 3 etapas
1º_  Desenvolvimento da api em node.js
2º_ Consumo da API em React
3º_ Estilização e validações

## Tela Login 
	O sistema possui uma tela de login para acessar o painel admin, onde é necéssário fazer um cadastro para proseguir.
	
## Tela Cadastro
	Input de email e senha, depois o usuário é redirecionado par a tela de login
	
## Painel admin
	Desenvolveu-se um pequeno painel para o usuário inserir os dados de registro como:
		Entrada, Saída para o almoço, Retorno do Almoço e Saída do trabalho.
		Os dados podem ser atualizados atualizados ao longo do dia, mas, ao completar todos os campos o usuário não poderá modificar os registros 
		cadastrados. Sendo assim, poderá realizar um novo registro  somente no dia seguinte.
		Só é possível fazer um cadastro por dia, para evitar registros com datas duplicadas.
	
## Comunicação 
	Utilizou-se a biblioteca axios para fazer a comunicação entre o front e o back, onde teremos o retornos das informações pertinentes.
	